package com.x8academy.Stack;
import org.junit.Assert;

public class StackLinked {
	StackElement topElement;
	
	public StackLinked(){
		topElement = null;
	}
	
	public void push(int input) {
		StackElement newElement = new StackElement(input);
		newElement.setPrevElement(topElement);
		topElement = newElement;
	}
	
	public int pop() {
		Assert.assertFalse(isEmpty());
		
		int returnValue = topElement.getValue();
		topElement = topElement.getPrevElement();
		
		return returnValue;
	}
	
	public int peek() {
		Assert.assertFalse(isEmpty());
		
		return topElement.getValue();
	}
	
	public boolean isEmpty() {
		return topElement == null;
	}
}

class StackElement {
	private int value;
	private StackElement prevElement;
	
	public StackElement(int input) {
		value = input;
		prevElement = null;
	}
	
	public int getValue() {
		return value;
	}
	
	public StackElement getPrevElement() {
		return prevElement;
	}
	
	public void setPrevElement(StackElement input) {
		prevElement = input;
	}
}