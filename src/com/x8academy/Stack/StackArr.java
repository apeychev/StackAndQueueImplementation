package com.x8academy.Stack;

import org.junit.Assert;

public class StackArr {

    private int[] source;
    private int elementCount;
    
    public StackArr() {
        source = new int[16];
        elementCount = 0;
    }
    
    public void push(int input) {
        if(elementCount == source.length) {
            int[] temp = new int[elementCount * 2];
            for (int i = 0; i < source.length; i++) {
                temp[i] = source[i];
            }
            source = temp;
        }
        source[elementCount++] = input;
    }
    
    public int pop() {
        Assert.assertFalse(isEmpty());
        //resize down
        
        
        
        return source[--elementCount];
    }
    
    public boolean isEmpty() {
        return elementCount == 0;
    }

    public int peek() {
        Assert.assertFalse(isEmpty());
        return source[elementCount - 1];
    }
    
    public static void main(String[] args) {
        
        StackArr instance = new StackArr();
        
        instance.peek();
        
    }
    
}

