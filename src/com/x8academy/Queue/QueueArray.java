package com.x8academy.Queue;

import org.junit.Assert;

public class QueueArray {
    private int[] storage;
    private int numberOfElements;
    private int headIndex;
    
    public QueueArray() {
        storage = new int[16];
        numberOfElements = 0;
        headIndex = 0;
    }
    
    public boolean isEmpty() {
        return numberOfElements == 0;
    }
    
    public void add(int input) {
        if(numberOfElements + headIndex == storage.length) {
            int[] temp = new int[storage.length * 2];
            for (int i = headIndex; i < storage.length; i++) {
                temp[i] = storage[i];
            }
            storage = temp;
        }
        
        storage[numberOfElements + headIndex] = input;
        numberOfElements++;
    }
    
    public int pop() {
        Assert.assertFalse(isEmpty());
        
        if(headIndex  >= storage.length / 2) {
            int[] temp = new int[storage.length / 2];
            for (int i = 0; i < numberOfElements; i++) {
                temp[i] = storage[headIndex + i];
            }
            storage = temp;
            headIndex = 0;
        }
        
        int temp = storage[headIndex++];
        numberOfElements--;
        return temp;
    }
    
    public int peek() {
        Assert.assertFalse(isEmpty());
        
        return storage[headIndex];
    }
    
    
}
