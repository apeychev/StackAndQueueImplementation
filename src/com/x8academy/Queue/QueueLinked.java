package com.x8academy.Queue;

import org.junit.Assert;

public class QueueLinked {

    private QueueElement head;
    private QueueElement tail;

    public QueueLinked() {
        head = null;
        tail = null;
    }

    public void add(int input) {
        QueueElement element = new QueueElement(input);

        if (isEmpty()) {
            head = element;
            tail = element;
        } else {
            tail.setNextElement(element);
            tail = element;
        }
    }

    public int pop() {
        Assert.assertFalse(isEmpty());
        int temp = head.getValue();
        head = head.getNextElement();
        return temp;
    }
    
    public int peek() {
        Assert.assertFalse(isEmpty());
        return head.getValue();
    }
    
    public boolean isEmpty() {
        if(head == null)
            tail = null;
        return head == null && tail == null;
    }

    public static void main(String[] args) {
        QueueLinked instance = new QueueLinked();
        instance.add(56);
        System.out.println(instance.tail.getValue());
        System.out.println(instance.head.getValue());
    }

}

class QueueElement {

    private int value;
    private QueueElement nextElement;

    public QueueElement(int value) {
        this.value = value;
        nextElement = null;
    }

    public QueueElement getNextElement() {
        return nextElement;
    }

    public void setNextElement(QueueElement nextElement) {
        this.nextElement = nextElement;
    }

    public int getValue() {
        return value;
    }

}
