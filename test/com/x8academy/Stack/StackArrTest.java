package com.x8academy.Stack;

import org.junit.Test;
import org.junit.Assert;

public class StackArrTest {

    @Test
    public void testPeekReturnLastPushedElement() {
        StackArr stackInstance = new StackArr();
        stackInstance.push(15);
        Assert.assertEquals(15, stackInstance.peek());
    }
    
    @Test (expected = AssertionError.class)
    public void testPeekWhenStackIsEmpty() {
        StackArr stackInstance = new StackArr();
        stackInstance.peek();
    }
    
    @Test
    public void testPushGivenIntegerWhenStackIsEmpty() {
        StackArr stackInstance = new StackArr();
        stackInstance.push(20);
        Assert.assertEquals(20, stackInstance.peek());
    }
    
    @Test
    public void testPushGivenIntegerWhenPushedMoreThanOneElement() {
        StackArr stackInstance = new StackArr();
        stackInstance.push(45);
        stackInstance.push(48);
        stackInstance.push(89);
        stackInstance.push(90);
        Assert.assertEquals(90, stackInstance.peek());
    }
    
    @Test
    public void testIsEmptyReturnTrue() {
        StackArr stackInstance = new StackArr();
        Assert.assertTrue(stackInstance.isEmpty());
    }
    
    @Test
    public void testIsEmptyReturnFalse() {
        StackArr stackInstance = new StackArr();
        stackInstance.push(17);
        Assert.assertFalse(stackInstance.isEmpty());
    }
    
    @Test(expected = AssertionError.class)
    public void testPopWhenStackIsEmpty() {
        StackArr stackInstance = new StackArr();
        stackInstance.pop();
    }
    
    @Test
    public void testPopWhenStackIsNotEmptyReturnTopElement() {
        StackArr stackInstance = new StackArr();
        stackInstance.push(78);
        stackInstance.push(89);
        Assert.assertEquals(89,stackInstance.pop());
    }

    @Test
    public void testPopWhenStackIsNotEmptyAndPoppedTopElement() {
        StackArr stackInstance = new StackArr();
        stackInstance.push(78);
        stackInstance.push(89);
        stackInstance.pop();
        Assert.assertEquals(78,stackInstance.peek());
    }

    @Test
    public void testPopWhenElementIsAddedAndPopped() {
        StackArr stackInstance = new StackArr();
        stackInstance.push(45);
        stackInstance.pop();
        Assert.assertTrue(stackInstance.isEmpty());
    }
    
}

