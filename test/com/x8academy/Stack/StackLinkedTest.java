package com.x8academy.Stack;

import org.junit.Test;
import org.junit.Assert;
import java.lang.AssertionError;


public class StackLinkedTest {

    @Test
    public void testPeekReturnLastPushedElement() {
        StackLinked stackInstance = new StackLinked();
        stackInstance.push(15);
        Assert.assertEquals(15, stackInstance.peek());
    }
    
    @Test (expected = AssertionError.class)
    public void testPeekWhenStackIsEmpty() {
        StackLinked stackInstance = new StackLinked();
        stackInstance.peek();
    }
    
    @Test
    public void testPushGivenIntegerWhenStackIsEmpty() {
        StackLinked stackInstance = new StackLinked();
        stackInstance.push(20);
        Assert.assertEquals(20, stackInstance.peek());
    }
    
    @Test
    public void testPushGivenIntegerWhenPushedMoreThanOneElement() {
        StackLinked stackInstance = new StackLinked();
        stackInstance.push(45);
        stackInstance.push(48);
        stackInstance.push(89);
        stackInstance.push(90);
        Assert.assertEquals(90, stackInstance.peek());
    }
    
    @Test
    public void testIsEmptyReturnTrue() {
        StackLinked stackInstance = new StackLinked();
        Assert.assertTrue(stackInstance.isEmpty());
    }
    
    @Test
    public void testIsEmptyReturnFalse() {
        StackLinked stackInstance = new StackLinked();
        stackInstance.push(17);
        Assert.assertFalse(stackInstance.isEmpty());
    }
    
    @Test(expected = AssertionError.class)
    public void testPopWhenStackIsEmpty() {
        StackLinked stackInstance = new StackLinked();
        stackInstance.pop();
    }
    
    @Test
    public void testPopWhenStackIsNotEmptyReturnTopElement() {
        StackLinked stackInstance = new StackLinked();
        stackInstance.push(78);
        stackInstance.push(89);
        Assert.assertEquals(89,stackInstance.pop());
    }

    @Test
    public void testPopWhenStackIsNotEmptyAndPoppedTopElement() {
        StackLinked stackInstance = new StackLinked();
        stackInstance.push(78);
        stackInstance.push(89);
        stackInstance.pop();
        Assert.assertEquals(78,stackInstance.peek());
    }

    @Test
    public void testPopWhenElementIsAddedAndPopped() {
        StackLinked stackInstance = new StackLinked();
        stackInstance.push(45);
        stackInstance.pop();
        Assert.assertTrue(stackInstance.isEmpty());
    }
    
}

