package com.x8academy.Queue;

import org.junit.Assert;
import org.junit.Test;



public class QueueLinkedTest {

    @Test
    public void testIsEmptyRetunTrue() {
        QueueLinked insatnce = new QueueLinked();
        Assert.assertTrue(insatnce.isEmpty());
    }
    
    @Test
    public void testIsEmptyRetunFalse() {
        QueueLinked insatnce = new QueueLinked();
        insatnce.add(23);
        Assert.assertFalse(insatnce.isEmpty());
    }
    
    @Test
    public void testPopWhenElementIsAddedAndPopped() {
        QueueLinked instance = new QueueLinked();
        instance.add(45);
        instance.pop();
        Assert.assertTrue(instance.isEmpty());
    }
    
    @Test
    public void testAddWhenQueueIsEmpty() {
        QueueLinked instance = new QueueLinked();
        instance.add(5);
        Assert.assertEquals(5, instance.peek());
    }
    
    @Test
    public void testAddWhenAddedMoreThanOneElement() {
        QueueLinked instance = new QueueLinked();
        instance.add(5);
        instance.add(65);
        instance.add(78);
        instance.add(8);
        Assert.assertEquals(5, instance.peek());
    }
    
    @Test(expected = AssertionError.class)
    public void testPeekWhenQueueIsEmpty() {
        QueueLinked instance = new QueueLinked();
        instance.peek();
    }
    
    @Test
    public void testPeekWhenQueueHasOneElement() {
        QueueLinked instance = new QueueLinked();
        instance.add(56);
        Assert.assertEquals(56, instance.peek());
    }
    
    @Test(expected = AssertionError.class)
    public void testPopWhenQueueIsEmpty() {
        QueueLinked instance = new QueueLinked();
        instance.pop();
    }
    
    @Test
    public void testPopWhenMoreThanOneElementsAreAdded() {
        QueueLinked instance = new QueueLinked();
        instance.add(67);
        instance.add(89);
        instance.add(45);
        instance.add(23);
        Assert.assertEquals(67, instance.pop());
    }
    
}
