package com.x8academy.Queue;

import org.junit.Assert;
import org.junit.Test;



public class QueueArrayTest {

    @Test
    public void testIsEmptyRetunTrue() {
        QueueArray insatnce = new QueueArray();
        Assert.assertTrue(insatnce.isEmpty());
    }
    
    @Test
    public void testIsEmptyRetunFalse() {
        QueueArray insatnce = new QueueArray();
        insatnce.add(23);
        Assert.assertFalse(insatnce.isEmpty());
    }
    
    @Test
    public void testPopWhenElementIsAddedAndPopped() {
        QueueArray instance = new QueueArray();
        instance.add(45);
        instance.pop();
        Assert.assertTrue(instance.isEmpty());
    }
    
    @Test
    public void testAddWhenQueueIsEmpty() {
        QueueArray instance = new QueueArray();
        instance.add(5);
        Assert.assertEquals(5, instance.peek());
    }
    
    @Test
    public void testAddWhenAddedMoreThanOneElement() {
        QueueArray instance = new QueueArray();
        instance.add(5);
        instance.add(65);
        instance.add(78);
        instance.add(8);
        Assert.assertEquals(5, instance.peek());
    }
    
    @Test(expected = AssertionError.class)
    public void testPeekWhenQueueIsEmpty() {
        QueueArray instance = new QueueArray();
        instance.peek();
    }
    
    @Test
    public void testPeekWhenQueueHasOneElement() {
        QueueArray instance = new QueueArray();
        instance.add(56);
        Assert.assertEquals(56, instance.peek());
    }
    
    @Test(expected = AssertionError.class)
    public void testPopWhenQueueIsEmpty() {
        QueueArray instance = new QueueArray();
        instance.pop();
    }
    
    @Test
    public void testPopWhenMoreThanOneElementsAreAdded() {
        QueueArray instance = new QueueArray();
        instance.add(67);
        instance.add(89);
        instance.add(45);
        instance.add(23);
        Assert.assertEquals(67, instance.pop());
    }
    
}
